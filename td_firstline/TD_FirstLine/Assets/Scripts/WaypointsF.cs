﻿using UnityEngine;

public class WaypointsF : MonoBehaviour
{
    public static Transform[] pointsF;

    void Awake ()
    {
        pointsF = new Transform[transform.childCount];
        for (int i = 0; i < pointsF.Length; i++)
        {
            pointsF[i] = transform.GetChild(i);
        }
    }
}
