﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float panSpeed = 30f;
    public float panBorderThickness = 10f;
    public float xMax;
    public float zMax;
    public float xMin;
    public float zMin;
    public float mouseSensitivity = 100f;
    public Transform cameraBody;

    void Update()
    {
        if (GameManager.gameIsOver)
        {
            this.enabled = false;
            return;
        }
        if (cameraBody.position.x >= xMax)
        {
            cameraBody.position = new Vector3(xMax,cameraBody.position.y, cameraBody.position.z);
        }
        if (cameraBody.position.z >= zMax)
        {
            cameraBody.position = new Vector3(cameraBody.position.x, cameraBody.position.y, zMax);
        }
        if (cameraBody.position.x <= xMin)
        {
            cameraBody.position = new Vector3(xMin,cameraBody.position.y, cameraBody.position.z);
        }
        if (cameraBody.position.z <= zMin)
        {
            cameraBody.position = new Vector3(cameraBody.position.x, cameraBody.position.y, zMin);
        }
    }
}
