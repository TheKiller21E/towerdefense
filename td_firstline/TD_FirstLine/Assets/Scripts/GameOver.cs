﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public Text roundsText;

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        MusicManager.gameMusic.Play();
    }

    public void Quit (string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
}
