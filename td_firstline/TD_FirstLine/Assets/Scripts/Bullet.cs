﻿ using UnityEngine;
 using System.Collections;

public class Bullet : MonoBehaviour
{

    private Transform target;

    public float speed = 70f;
    public int bulletDmg = 20;
    public void Seek(Transform _target)
    {
        target = _target;
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(bulletSeek());        
    }
    

    void HitTarget()
    {
        Damage(target);
        Destroy(gameObject);
    }

    void Damage(Transform enemy)
    {
        Enemy e = enemy.GetComponent<Enemy>();

        e.TakeDamage(bulletDmg);
    }
    IEnumerator bulletSeek()
    {
        if (target == null)
        {
                Destroy(gameObject);                   
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();           
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);
        yield break;
    }
}
