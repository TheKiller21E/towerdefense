﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    private static MusicManager instance = null;
    public AudioMixer audioMixer;
    public static AudioSource gameMusic;
    public static AudioSource introMusic;
    [SerializeField]
    private GameObject _gameMusic;
    [SerializeField]
    private GameObject _introMusic;
    [SerializeField]
    private List<AudioSource> _audioSource;
    public static AudioClip[] audioClip;
    public GameObject _fuente;
    public AudioClip[] _sonidos;
    private void Awake()
    {
        audioClip = _sonidos;
        //_audioSource.clip = null;
        if (instance == null)
        {
            for (int i = 0; i < _sonidos.Length; i++)
            {
                AudioSource P = _fuente.AddComponent<AudioSource>();
                P.clip = _sonidos[i];
                _audioSource.Add(P);
            }
            instance = this;
            Instantiate(_introMusic);
            Instantiate(_gameMusic);
            DontDestroyOnLoad(gameObject);
            gameMusic = GameObject.FindGameObjectWithTag("GameMusic").GetComponent<AudioSource>();
            introMusic = GameObject.FindGameObjectWithTag("IntroMusic").GetComponent<AudioSource>();
            setGameFalse();
            DontDestroyOnLoad(gameMusic);
            DontDestroyOnLoad(introMusic);
        }        
    }
    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("Volume", volume);
    }
    public static void setIntroFalse()
    {
        introMusic.Stop();
        gameMusic.Play();
    }
    public static void setGameFalse()
    {
        gameMusic.Stop();
        introMusic.Play();
    }
    public void playClip(int _clipNum)
    {      
        _audioSource[_clipNum].Play();
    }
    public void playClipTorret(int _clipNum)
    {
        if (_audioSource[_clipNum].isPlaying == false)
        {
            _audioSource[_clipNum].Play();
        }
    }
    public void stopClip(int _clipNum)
    {
        _audioSource[_clipNum].Stop();
    }
}