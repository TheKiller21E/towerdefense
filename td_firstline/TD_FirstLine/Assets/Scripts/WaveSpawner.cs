using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{
    public static int EnemiesAlive = 0;
    //public Transform enemyPrefab;
    
    public GameObject[] enemies;
    public List<Waves> waves = new List<Waves>();
    public static int basicCount = 0;
    public static int toughCount = 0;
    public static int fastCount = 0;
    public static int flyCount = 0;
    public int basicCountS = 0;
    public int toughCountS = 0;
    public int fastCountS = 0;
    public int flyCountS = 0;
    public static int totalCount = 0;

    [Tooltip("Tutorial")]
    public bool _Tutorial;
    public GameObject _uiTutorial;
    public Text Be_ePTEXT;
    public float delay = 0.1f;
    public string _fullText;


    public int TotalCount = 0;
    public static int currentWave = 0;
    public static int aliveCount = 0;
    public Transform spawnPoint;
    public float timeBetweenWaves = 5f;
    private float countdown = 2f;
    public Text waveCountDownText;

    private void Start()
    {
        currentWave = 0;
        basicCount = 0;
        toughCount = 0;
        fastCount = 0;
        flyCount = 0;
        aliveCount = 0;
        totalCount = 0;
        countdown = 2f;
        if (_Tutorial == false)
        {
            StartCoroutine(StartEnemies());
        }
        else
        {
            StartCoroutine(StartTutorial());
        } 
    }
    void Update()
    {
        TotalCount = totalCount;
        if (_Tutorial == false)
        {            
            if (aliveCount > 0)
            {
                return;
            }
            if (countdown <= 0f)
            {               
                StartCoroutine(SpawnWave());
                countdown = timeBetweenWaves;
                //return;                         
            }
            if (GameManager.gameIsOver)
            {
                this.enabled = false;
            }
            if (currentWave >= waves.Count)
            {
                StopAllCoroutines();
                GameManager.LevelWon();                
            }
            countdown -= Time.deltaTime;
            countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);
            waveCountDownText.text = string.Format("{0:00}", countdown);
        }        
    }
    IEnumerator SpawnWave()
    {
        _Tutorial = false;
        //waveIndex++;
        Debug.Log("Vamos por la ronda " + (currentWave + 1));
        PlayerStats.Rounds++;
        for (int i = 0; i < waves[currentWave].totalEnemies; i++)
        {
            if (basicCount >= 5)
            {
                SpawnTough();
                totalCount++;
                //toughCount++;
            }
            else if (toughCount >= 3)
            {
                SpawnFast();
                totalCount++;
                //fastCount++;
            }
            else if (fastCount >= 3)
            {
                SpawnFly();
                totalCount++;
                //flyCount++;
                
            }
            else if (basicCount >= 0)
            {
                SpawnEnemy();
                totalCount++;
                yield return new WaitForSeconds(0.75f);
            }            
        }
        currentWave++;                      
    }
    IEnumerator StartEnemies()
    {
        if (toughCountS > 0)
        {
            for (int i = 0; i < toughCountS; i++)
            {
                Instantiate(enemies[1], spawnPoint.position, spawnPoint.rotation);
                aliveCount++;
                toughCount++;
                yield return new WaitForSeconds(0.75f);
            }
        }
        if (fastCountS > 0)
        {
            for (int i = 0; i < fastCountS; i++)
            {
                Instantiate(enemies[2], spawnPoint.position, spawnPoint.rotation);
                aliveCount++;
                fastCount++;
                yield return new WaitForSeconds(0.75f);
            }
        }
        if (flyCountS > 0)
        {
            for (int i = 0; i < flyCountS; i++)
            {
                Instantiate(enemies[3], spawnPoint.position, spawnPoint.rotation);
                aliveCount++;
                flyCount++;
                yield return new WaitForSeconds(0.75f);
            }
        }
        if (basicCountS > 0)
        {
            for (int i = 0; i < basicCountS; i++)
            {
                Instantiate(enemies[0], spawnPoint.position, spawnPoint.rotation);
                basicCount++;
                aliveCount++;
                yield return new WaitForSeconds(0.75f);
            }
        }
    }
    void SpawnEnemy()
    {
        Instantiate(enemies[0], spawnPoint.position, spawnPoint.rotation);
        basicCount++;
        aliveCount++;
        
    }

    void SpawnTough()
    {
        Instantiate(enemies[1], spawnPoint.position, spawnPoint.rotation);
        basicCount = 0;
        aliveCount++;
        toughCount++;
    }
    void SpawnFast()
    {
        Instantiate(enemies[2], spawnPoint.position, spawnPoint.rotation);
        toughCount = 0;
        aliveCount++;
        fastCount++;
    }
    void SpawnFly()
    {
        var offset = new Vector3(0f, 20f, 0f);
        Instantiate(enemies[3], spawnPoint.position + offset, spawnPoint.rotation);
        fastCount = 0;
        aliveCount++;
        flyCount++;

    }
    public List<Waves> GetList()
    {
        return waves;
    }
    IEnumerator StartTutorial()
    {
        _uiTutorial.SetActive(true);
        FindObjectOfType<MusicManager>().playClip(5);
        foreach (char letter in _fullText.ToCharArray())
        {            
            Be_ePTEXT.text += letter;            
            yield return new WaitForSeconds(delay);
        }
        FindObjectOfType<MusicManager>().stopClip(5);
        _Tutorial = false;
        _uiTutorial.SetActive(false);
    }
}