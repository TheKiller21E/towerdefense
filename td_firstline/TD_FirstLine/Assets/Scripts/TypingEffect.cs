﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypingEffect : MonoBehaviour
{
    public Text _typeo;
    public float delay = 0.1f;
    public string fullText;
    [SerializeField]
    private GameObject _select;    
    [SerializeField]
    private GameObject _botones;
    [SerializeField]
    private GameObject[] _buttons;
    public Button[] _levelButtons;
    void Awake()
    {
        Time.timeScale = 1f;
        StartCoroutine(ShowText());
        _typeo = this.GetComponent<Text>();
        int levelReached = PlayerPrefs.GetInt("levelReached", 1);
        for (int i = 0; i < _levelButtons.Length; i++)
        {
            if (i + 1 > levelReached)
            {
                _levelButtons[i].interactable = false;
            }            
        }
    }
    private void Update()
    {
        if (Input.anyKeyDown)
        {
            if (_typeo.text != fullText)
            {
                StopAllCoroutines();
                FastButtons();
                _select.SetActive(true);
                StartCoroutine(Blink());
                _typeo.text = fullText;
            }          
        }        
    }

    IEnumerator ShowText()
    {
        
        foreach (char letter in fullText.ToCharArray())
        {
            _typeo.text += letter;
            FindObjectOfType<MusicManager>().playClip(4);
            yield return new WaitForSeconds(delay);
        }           
        _select.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(Blink());
        StartCoroutine(ShowButtons());
        
    }
    IEnumerator Blink()
    {
        _typeo.color = new Color(_typeo.color.r, _typeo.color.g, _typeo.color.b, 1);
        yield return new WaitForSeconds(0.8f);
        while (true)
        {
            switch (_typeo.color.a.ToString())
            {
            case "0":
                    _typeo.color = new Color(_typeo.color.r, _typeo.color.g, _typeo.color.b, 1);                
                yield return new WaitForSeconds(0.8f);
                break;
            case "1":
                    _typeo.color = new Color(_typeo.color.r, _typeo.color.g, _typeo.color.b, 0);
                yield return new WaitForSeconds(0.8f);
                    break;
            }          
        }
    }
    IEnumerator ShowButtons()
    {
        _botones.SetActive(true);
        for (int i = 0; i < _buttons.Length; i++)
        {
            _buttons[i].SetActive(true);
            yield return new WaitForSeconds(0.5f);
        }
    }
    public void FastButtons()
    {
        _botones.SetActive(true);
        _buttons[0].SetActive(true);
        _buttons[1].SetActive(true);
        _buttons[2].SetActive(true);
        _buttons[3].SetActive(true);
        _buttons[4].SetActive(true);
    }
}
