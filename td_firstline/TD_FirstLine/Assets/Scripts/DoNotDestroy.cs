﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoNotDestroy : MonoBehaviour
{
    [SerializeField]
    GameObject[] intrObj;
    [SerializeField]
    GameObject[] gamemusicObj;
    private void Awake()
    {
        intrObj = GameObject.FindGameObjectsWithTag("IntroMusic");
        gamemusicObj = GameObject.FindGameObjectsWithTag("GameMusic");

        if (intrObj.Length > 1)
        {
            //intrObj[1].SetActive(false); ;
        }
        else
            return;
        if (gamemusicObj.Length > 1)
        {
            //gamemusicObj[1].SetActive(false);
        }
        else
            return;
        DontDestroyOnLoad(intrObj[0]);
        DontDestroyOnLoad(gamemusicObj[0]);
    }
}
