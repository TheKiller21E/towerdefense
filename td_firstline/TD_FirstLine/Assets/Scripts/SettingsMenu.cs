﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.UI;


public class SettingsMenu : MonoBehaviour
{
    public GameObject UIsett;
    Resolution[] resolutions;
    public Dropdown resolutionDropdown;
    public static bool _devTools = false;
    void Start()
    {
       resolutions = Screen.resolutions;
       resolutionDropdown.ClearOptions();
       List<string> options = new List<string>();
        int currentResolutionIndex = 0;
       for (int i = 0; i < resolutions.Length; i++)
       {
           string option = resolutions[i].width + " x " + resolutions[i].height;
           options.Add(option);
            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
       }
       resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
    }
    public void SetResolution(int resolutionIndex)
    {        
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);        
    }

    public void SettingActive()
    {
        //MusicManager.playClip(3);
        FindObjectOfType<MusicManager>().playClip(3);
        UIsett.SetActive(!UIsett.activeSelf);
    }
    public void SceneChange(string SceneName)
    {
        // MusicManager.playClip(3);
        //FindObjectOfType<MusicManager>().playClip(3);
        SceneManager.LoadScene(SceneName);
    }
    
    public void SetFullScreen(bool isFullscreen)
    {
        // MusicManager.playClip(3);
        FindObjectOfType<MusicManager>().playClip(3);
        Screen.fullScreen = isFullscreen;
    }
    public void Exit()
    {
        Application.Quit();
    }
}
