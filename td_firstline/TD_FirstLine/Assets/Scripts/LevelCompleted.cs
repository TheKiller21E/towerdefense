﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelCompleted : MonoBehaviour
{
    public string menuSceneName = "LevelMenu";

    public string nextLevel = "Level02";
    public int levelToUnlock;
    public void Continue()
    {
        PlayerPrefs.SetInt("levelReached", levelToUnlock);
        SceneManager.LoadScene(nextLevel);
    }
    public void Menu()
    {
        PauseMenu.Quit(menuSceneName);
    }
}
