﻿using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float speed = 10f;

    public float health = 100;
    public int moneyAtDead = 10;
    private Transform target;
    private int wavepointIndex = 0;
    //public int maxHealth = 100;
    public int enemyDamage = 0;
    public Image healthBar;
    /*public GameObject _particlePrefab;
    ParticleSystem _particle;
    ParticleSystem.EmissionModule em;*/
    void Start()
    {        
        target = Waypoints.points[0]; 
        /*_particle = _particlePrefab.GetComponent<ParticleSystem>();
        em = _particlePrefab.GetComponent<ParticleSystem>().emission;*/
    }

    public void  TakeDamage(float amount)
    {
        health -= amount;
        healthBar.fillAmount = health / 100f;
        if (health <= 0)
        {
            //_particle.Play();
            //em.enabled = true;
            Die();
        }
    }

    void Die()
    {
        PlayerStats.Money = PlayerStats.Money + moneyAtDead;
        //WaveSpawner.EnemiesAlive--;
        WaveSpawner.aliveCount--;
        WaveSpawner.totalCount--;
        Destroy(gameObject);
    }

    void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);
        transform.LookAt(target);

        if (Vector3.Distance(transform.position, target.position) <= 0.8f)
        {
            GetNextWayPoint();
        }
       
    }
    public void GetNextWayPoint()
    {
        if (wavepointIndex >= Waypoints.points.Length - 1)
        {
            //MusicManager.playClip(2);
            FindObjectOfType<MusicManager>().playClip(2);
            PlayerStats.currentHealth -= enemyDamage;
            Destroy(gameObject);
            WaveSpawner.aliveCount--;
            return;
        }
        wavepointIndex++;
        target = Waypoints.points[wavepointIndex];
    }
}
