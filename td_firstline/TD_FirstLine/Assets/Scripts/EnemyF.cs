﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyF : MonoBehaviour
{
    public float speed = 10f;

    public float health = 100;
    public int moneyAtDead = 10;
    private Transform target;
    private int wavepointIndex = 0;
    //public int maxHealth = 100;
    public int enemyDamage = 0;
    public Image healthBarF;

    private void Start()
    {
        target = WaypointsF.pointsF[0];
    }
    public void TakeDamageF(float amount)
    {
        health -= amount;
        healthBarF.fillAmount = health / 100f;
        if (health <=0)
        {
            DieF();
        }
    }
    void DieF()
    {
        PlayerStats.Money = PlayerStats.Money + moneyAtDead;
        WaveSpawner.aliveCount--;
        WaveSpawner.totalCount--;
        Destroy(gameObject);
    }
    
    void Update()
    {
        Vector3 dirF = target.position - transform.position;
        transform.Translate(dirF.normalized * speed * Time.deltaTime, Space.World);
        transform.LookAt(target);
        if (Vector3.Distance(transform.position, target.position) <= 0.8f)
        {
            GetNextWayPointF();
        }
    }
    public void GetNextWayPointF()
    {
        if (wavepointIndex>= WaypointsF.pointsF.Length - 1)
        {
            PlayerStats.currentHealth -= enemyDamage;
            WaveSpawner.aliveCount--;
            Destroy(gameObject);
            return;
        }
        wavepointIndex++;
        target = WaypointsF.pointsF[wavepointIndex];
    }
}
