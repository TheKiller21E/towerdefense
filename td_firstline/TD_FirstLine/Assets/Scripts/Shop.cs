﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public TurretBlueprint standardTurret;
    public TurretBlueprint sniperTurret;
    public TurretBlueprint antiairTurret;
    BuildManager buildManager;
    private void Start()
    {
        buildManager = BuildManager.instance;
    }
    public void SelectStandardTurret()
    {
        Debug.Log("Comprada");
        buildManager.SelectTurretToBuild(standardTurret);
    }
    public void SelectSniperTurret()
    {
        Debug.Log("Comprada otra");
        buildManager.SelectTurretToBuild(sniperTurret);
    }
    public void SelectAntiairTurret()
    {
        buildManager.SelectTurretToBuild(antiairTurret);
    }
}
