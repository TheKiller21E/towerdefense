﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static bool gameIsOver;

    public GameObject gameOverUI;
    public static GameObject levelWonUI;
    public GameObject levelWinUI;

    private void Start()
    {
        gameIsOver = false;
        Time.timeScale = 1f;
        levelWonUI = levelWinUI;
    }
    void Update()
    {
        if (gameIsOver)
        {
            return;
        }
        if (PlayerStats.currentHealth <= 0)
        {
            EndGame();
        }
    }

    void EndGame()
    {
        FindObjectOfType<MusicManager>().playClip(6);
        gameIsOver = true;
        MusicManager.gameMusic.Stop();
        gameOverUI.SetActive(true); 
        Time.timeScale = 0f;
    }
    public static void LevelWon()
    {
        gameIsOver = true;
        levelWonUI.SetActive(true);
        Time.timeScale = 0f;
    }
}
