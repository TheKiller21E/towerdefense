﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject ui;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
        {
            Toggle();
        }
    }

    public void Toggle()
    {        
        ui.SetActive(!ui.activeSelf);

        if (ui.activeSelf)
        {
            Time.timeScale = 0f;
            MusicManager.gameMusic.Pause();
        }
        else
        {
            Time.timeScale = 1f;
            MusicManager.gameMusic.Play();
        }
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void Continue()
    {
        Toggle();
    }
    public static void Quit(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
        MusicManager.setGameFalse();
    }    
    public void Salir(string SceneName)
    {
        PauseMenu.Quit(SceneName);
    }
}
