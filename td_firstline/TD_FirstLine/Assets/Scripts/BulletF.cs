﻿ using UnityEngine;

public class BulletF : MonoBehaviour
{

    private Transform target;

    public float speed = 70f;
    public int bulletDmg = 20;

    public void Seek(Transform _target)
    {
        target = _target;
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);
    }

    void HitTarget()
    {
        Damage(target);
        Destroy(gameObject);
    }

    void Damage(Transform enemy)
    {
        EnemyF e = enemy.GetComponent<EnemyF>();

        e.TakeDamageF(bulletDmg);
    }
}
