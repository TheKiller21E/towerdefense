﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelSelector : MonoBehaviour
{
    public void LoadScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
        MusicManager.setIntroFalse();
    }
    public void Quit(string SceneName)
    {
        PauseMenu.Quit(SceneName);
    }
}
